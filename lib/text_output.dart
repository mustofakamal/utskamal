import 'package:flutter/material.dart';
import './text_control.dart';

class TextOutput extends StatefulWidget {
  @override
  _OutputState createState() => _OutputState();
}

class _OutputState extends State<TextOutput> {
  String msg = 'Temanggung';

  void _changeText() {
    setState(() {
      if (msg.startsWith('T')) {
        msg = 'Kudus';
      } else if (msg.startsWith('B')) {
        msg = 'Temanggung';
      } else {
        msg = 'Bawen';
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            'Nama Kota : ',
            style: new TextStyle(fontSize: 35.0),
          ),
          Control(msg),
          RaisedButton(
            child: Text(
              "Pindah Kota",
              style: new TextStyle(color: Colors.green[200], fontSize: 20.0),
            ),
            color: Theme.of(context).primaryColor,
            onPressed: _changeText,
          ),
        ],
      ),
    );
  }
}
