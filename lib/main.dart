import 'package:flutter/material.dart';
import './text_output.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
          brightness: Brightness.dark, primarySwatch: Colors.deepOrange),
      debugShowCheckedModeBanner: false,
      title: 'UTS MOBILE',
      home: Scaffold(
        appBar: AppBar(
          actions: <Widget>[
            new Icon(Icons.search),
            new Icon(Icons.more_vert),
          ],
          leading: new Icon(Icons.home),
          title: Text('UTS MOBILE'),
        ),
        body: TextOutput(),
      ),
    );
  }
}
